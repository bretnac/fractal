#lang racket/gui

; Cantor comb (Figure 2.2)

(require "canvas.rkt")

; Dimensions of canvas in pixels
(define w 1000)
(define h 1000)

(define height 20) ; height of each layer
(define scale 1/3) ; ratio of widths between succesisve layers

(define (draw-cantor dc x y width)
  (send dc draw-rectangle x y width height)
  (printf "x0 ~a y0 ~a w ~a ~n" x y width)
  (cond ((> width 2)
         (let ((next-width (exact-round (* width scale))))
           (draw-cantor dc x (+ y height) next-width)
           (draw-cantor dc (+ x width (- next-width)) (+ y height) next-width))
         )
    )
  )

(define (draw-fractal draw-fn length plot-label x y)
  (let ((frame (new frame%
                    [label plot-label]
                    [width w]
                    [height h])))
    (new canvas% [parent frame]
         [paint-callback
          (lambda (canvas dc)
            (send dc set-brush (make-brush))
            (draw-fn dc x y length)
            )])
    (send frame show #t)
    ))

(draw-fractal draw-cantor (round (* w 0.9)) "COMB" 10 10) 