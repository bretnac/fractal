#lang racket/gui

(provide w h plot)

(define w 600)
(define h 600)

(define cx (/ w 2))
(define cy (/ h 2))

(define (plot draw-branch length depth plot-label)
  (let ((frame (new frame%
                    [label plot-label]
                    [width w]
                    [height h])))
    (new canvas% [parent frame]
         [paint-callback
          (lambda (canvas dc)
            (draw-branch dc cx cy length depth)
            )])
    (send frame show #t)
    ))
